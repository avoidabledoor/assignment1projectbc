import React from 'react'
import {BottomNavigation, BottomNavigationAction} from '@material-ui/core';
import FacebookIcon from "@material-ui/icons/Facebook";
import InstagramIcon from "@material-ui/icons/Instagram";
import TwitterIcon from "@material-ui/icons/Twitter";
import YoutubeIcon from "@material-ui/icons/YouTube";

function Footer() {
    return (
        <BottomNavigation>
            <BottomNavigationAction label="Facebook" icon={<FacebookIcon style={{fill:"#4267B2", height:"50", width:"50"}}/>}/>
            <BottomNavigationAction label="Youtube" icon={<YoutubeIcon style={{fill:"#FF0000", height:"50", width:"50"}}/>}/>
            <BottomNavigationAction label="Instagram" icon={<InstagramIcon style={{fill:"#bc2a8d", height:"50", width:"50"}}/>}/>
            <BottomNavigationAction label="Twitter" icon={<TwitterIcon style={{fill:"#1DA1F2", height:"50", width:"50"}}/>}/>
        </BottomNavigation>
    )
}

export default Footer
