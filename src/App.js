import { createTheme, ThemeProvider, makeStyles, Typography } from '@material-ui/core';
import ReactPlayer from 'react-player'
import NavBar from './components/NavBar'
import Grid from './components/Grid'
import Footer from './components/Footer'
import './App.css';
import nightPic from './nightSky.jpg';

import SecurityIcon from '@material-ui/icons/Security';
import EventNoteIcon from '@material-ui/icons/EventNote';
import HttpIcon from '@material-ui/icons/Http';
import ImportExportIcon from '@material-ui/icons/ImportExport';
import TrendingUpIcon from '@material-ui/icons/TrendingUp';
import ComputerIcon from '@material-ui/icons/Computer';

const theme = createTheme({
  pallete:{
    primary:{
      main:"#344B51",
    },
    secondary:{
      main:"#A0A587",
    },
  },
  typography: {
    fontFamily: [
      'Roboto'
    ],
    h3: {
      fontWeight: 1000,
      fontSize: 57,
      lineHeight: '2rem',
    },
    h4: {
      fontWeight: 400,
      fontSize: 20,
      lineHeight: '2rem',
    },
    h5:{
      fontWeight: 100,
      lineHeight: '2rem',
    },
  },
});

const styles = makeStyles({
  wrapper:{
    width:"65%",
    margin:"auto",
    textAlign:"center"
  },
  bigSpace:{
    marginTop:"5rem",
    position: "relative",
    left: "0",
    top: "0",
    transform: "none"

  },
  littleSpace:{
    marginTop:"2.5rem",
  },
  grid:{
    display:"flex",
    justifyContent:"center",
    alignItems:"center",
    flexWrap: "wrap",
  }
})

function App() {
  const classes = styles();
  return (
    <div className="App">
      <ThemeProvider theme={theme}>
      <NavBar/>
      <div className={classes.wrapper}>
        <Typography variant="h3" className={classes.bigSpace} color="primary">
          What is Lorem Ipsum?
        </Typography>
        <Typography variant="h5" className={classes.littleSpace} color="primary">
        Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum
        </Typography>
      </div>
      <div className={classes.littleSpace}>
        <div className={classes.wrapper}>
        <Typography variant="h4" className={classes.littleSpace} color="primary">
        Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of "de Finibus Bonorum et Malorum" (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, "Lorem ipsum dolor sit amet..", comes from a line in section 1.10.32.
        </Typography>
        <Typography variant="h5" className={classes.littleSpace} color="primary">
        The standard chunk of Lorem Ipsum used since the 1500s is reproduced below for those interested. Sections 1.10.32 and 1.10.33 from "de Finibus Bonorum et Malorum" by Cicero are also reproduced in their exact original form, accompanied by English versions from the 1914 translation by H. Rackham.
        </Typography>
        </div>
      </div>
      <div className={`${classes.grid} ${classes.bigSpace}`}>
        <Grid icon={<ImportExportIcon style={{fill: "#DF5654", height:"125", width:"125"}}/>} title="Arrows" btnTitle="Show Me More"/>
        <Grid icon={<TrendingUpIcon style={{fill: "#5B5C48", height:"125", width:"125"}}/>} title="We makin money" btnTitle="Show Me More"/>
        <Grid icon={<EventNoteIcon style={{fill: "#F3A444", height:"125", width:"125"}}/>} title="Mail" btnTitle="Show Me More"/>
      </div>
      <div className={classes.bigSpace}>
        <ReactPlayer  controls url='https://www.youtube.com/watch?v=Ik7p6vvvWOQ'/>
        <img  src={nightPic} alt="Pretty night sky with lots of stars. Easy to get lost. Like sugar sprinkled on the hood of a black car in the moonlight the shimmer and glisten."/>
      </div>
      <div className={classes.bigSpace}>
        <Footer/>
      </div>
      </ThemeProvider>
    </div>
  );
}

export default App;
